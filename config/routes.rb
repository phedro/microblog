Rails.application.routes.draw do
  root to: 'users#index'
  resources :posts

  devise_for :users
  resources :users do
    member do
      post 'follow/:following_id' => 'users#follow', as: :follow
      delete 'unfollow/:following_id' => 'users#unfollow', as: :unfollow
    end
  end
end
