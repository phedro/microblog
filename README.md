# Microblog

um microblog, assim como o twitter.

## O usuário deve poder

- Realizar o login(email/senha, OAuth, passwordless como preferir) [DONE]
- Realizar o logout [DONE]

## O usuário logado deve poder

- Criar postagens [DONE]
- Ver as postagens criadas [DONE]
- Buscar por outros usuários
- Visualizar o perfil de outros usuários com suas postagens [DONE]
- Seguir outro usuário [DONE]
- Receber notificação ao ser seguido por um usuário

## O usuário não logado deve poder

- Ver um perfil criado [DONE]
- Ver as postagens do perfil [DONE]

## Vamos considerar:

- Testes unitários e de aceitação. Rspec, TestUnit, ou o que preferir.
- Design de código
- Boas práticas
- Utilização dos recursos da linguagem
- Conhecimento de Orientação a objeto

## Diferenciais:

- Utilizar boas mensagens de commit
- 100% de cobertura de testes
- Hospedar a aplicação no heroku [DONE] http://phedro-microblog.herokuapp.com
- Aplicar um front-end apresentável