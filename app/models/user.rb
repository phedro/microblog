class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :rememberable, :validatable
  validates :username, presence: true, length: { maximum: 30 }

  has_many :post, dependent: :destroy
  has_many :follower_users, class_name: 'Follow', foreign_key: :following_id
  has_many :following_users, class_name: 'Follow', foreign_key: :follower_id
  has_many :followers, through: :follower_users, source: :follower
  has_many :followings, through: :following_users, source: :following
end
